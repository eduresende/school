def create_clazz lesson, teacher
  Clazz.create! lesson: lesson, teacher: teacher
end