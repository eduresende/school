app_dir = "/home/ubuntu/apps/school"

worker_processes 2

working_directory "#{app_dir}/current" # available in 0.94.0+

listen "/tmp/unicorn.school.sock", :backlog => 64
listen 3000, :tcp_nopush => true

timeout 30

pid "#{app_dir}/shared/pids/unicorn.pid"

stderr_path "#{app_dir}/shared/log/unicorn_errors.log"
stdout_path "#{app_dir}/shared/log/unicorn.log"