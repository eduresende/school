require 'spec_helper'


describe ClassroomsHelper do

  it "tables_lines" do
    classroom = Classroom.new
    helper.tables_lines(classroom).should == 0
    classroom.students << Student.new
    helper.tables_lines(classroom).should == 1
    classroom.students << Student.new
    helper.tables_lines(classroom).should == 2
    classroom.clazzs << Clazz.new
    helper.tables_lines(classroom).should == 2
    classroom.clazzs << Clazz.new
    helper.tables_lines(classroom).should == 2
    classroom.clazzs << Clazz.new
    helper.tables_lines(classroom).should == 3
  end
end
