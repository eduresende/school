jQuery ->
  bindRemoveClass = undefined
  bindRemoveStudent = undefined
  bindRemoveClass = ->
    $(".remove_clazz").unbind().click ->
      lessonId = $(this).data("lesson-id")
      $(this).parent().parent().remove()
      $.ajax
        url: "/lessons/" + lessonId + ".json"
        success: (data) ->
          $("#lessons").append("<option value=\"" + data.id + "\">" + data.name + "</option>").trigger "liszt:updated"
          $("#lessons").trigger "change"
          false

      false

    false

  bindRemoveStudent = ->
    $(".remove_student").unbind().click ->
      studentId = $(this).data("student-id")
      $(this).parent().parent().remove()
      $.ajax
        url: "/students/" + studentId + ".json"
        success: (student) ->
          $("#students").append new Option(student.name, student.id)
          $("#students").trigger "liszt:updated"
      false

    false

  $("#add_clazz").click ->
    lessionId = undefined
    lessionId = $("#lessons").val()
    return false  unless lessionId
    $.ajax
      url: "/classrooms/clazz_view"
      data:
        lesson_id: lessionId
        teacher_id: $("#teachers").val()

      success: (data) ->
        lessonSelect = undefined
        $("#clazz_container").append data
        lessonSelect = $("#lessons")
        lessonSelect.find("option[value=\"" + lessionId + "\"]").remove()
        lessonSelect.trigger "liszt:updated"
        $("#lessons").trigger "change"
        bindRemoveClass()

    false

  $("#lessons").chosen()
  $("#teachers").chosen()
  $("#lessons").change ->
    id = $(this).val()
    unless id
      $("#teachers").html("").trigger "liszt:updated"
      return
    return false  if $("#lessons").attr("data-last") is id
    $("#lessons").attr "data-last", id
    $.ajax
      url: "/teachers/for_lesson/" + id
      dataType: "script"

    false

  $("#add_student").click ->
    select = $("#students")
    studentId = select.val()
    select.find("option:selected").remove()
    $.ajax
      url: "/students/for_classroom/" + studentId
      success: (data) ->
        $("#students_container").append data
        bindRemoveStudent()

    select.trigger "liszt:updated"
    false


  bindRemoveStudent()
  bindRemoveClass()
  $("#students").chosen()
  $("#lessons").trigger "change"

  $('#teacher_filter').tokenInput '/teachers/query.json',
      prePopulate: $('#teacher_filter').data('load')

  $('#lesson_filter').tokenInput '/lessons/query_without_new.json',
      prePopulate: $('#lesson_filter').data('load')
  true