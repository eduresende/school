module TeachersHelper
  def lessons_links_for_teacher teacher
    teacher.lessons.map{ |lesson| lesson_link(lesson)}.join(", ").html_safe
  end

  def lesson_link lesson
    link_to lesson, lesson_path(lesson)
  end
end

