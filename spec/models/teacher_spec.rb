require 'spec_helper'

describe Teacher do

  context "validations" do
    it "should require name" do
      @teacher = Teacher.new
      @teacher.should_not be_valid
      @teacher.name = "name"
      @teacher.should be_valid
    end
  end
  it "should return lessons_string" do
    english = Lesson.new(name: "english")
    sciences = Lesson.new(name: "sciences")
    geography = Lesson.new(name: "geography")

    teacher = Teacher.new
    teacher.lessons = [english, sciences, geography]

    teacher.lessons_string.should == "english, sciences, geography"
  end


end
