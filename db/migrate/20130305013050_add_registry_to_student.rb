class AddRegistryToStudent < ActiveRecord::Migration
  def change
    add_column :students, :registry, :integer
  end
end
