require 'spec_helper'

describe Student do

  context "validations" do
    before do
      @student = Student.new
      @student.name = "student"
      @student.registry=1234

      @student.should be_valid
    end

    after do
      @student.destroy
    end

    it "should require name" do
      @student.name = ""
      @student.should_not be_valid
      @student.errors[:name].should be_present
    end

    it "should require registry" do
      @student.registry = nil
      @student.should_not be_valid
      @student.errors[:registry].should be_present
    end

    it "should require unique registry" do
      @student.save!
      new_student = Student.new
      new_student.name = "student"
      new_student.registry=1234

      new_student.should_not be_valid
    end
  end
end
