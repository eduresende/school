class Lesson < ActiveRecord::Base
  #materia
  attr_accessible :name

  validates_uniqueness_of :name

  has_many :clazzs
  has_many :teachers, :through => :clazzs
  has_many :classrooms, :through => :clazzs

  scope :with_teachers, joins(:clazzs).joins(:teachers).uniq
  scope :with_teachers_and_no_classrooms,
        joins("LEFT JOIN clazzs ON clazzs.lesson_id = lessons.id").
        joins("INNER JOIN teachers ON clazzs.teacher_id = teachers.id").
        where("clazzs.id NOT IN (SELECT clazz_id FROM classrooms_clazzs)").uniq
  scope :sorted, order(:name)
  scope :query_without_new, lambda { |q| order(:name).where("name like ?", "%#{q}%")}

  def self.query query
    lessons = order(:name).where("name like ?", "%#{query}%")
    if lessons.empty?
      [{id: "<<<#{query.capitalize}>>>", name: "#{I18n.t('words.new')}: \"#{query.capitalize}\""}]
    else
      lessons.to_a
    end
  end

  def self.ids_from_names names
    names.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    names.split(',')
  end

  def to_s
    name
  end
end
