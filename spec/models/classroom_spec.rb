require 'spec_helper'

describe Classroom do

  context "validations" do
    before do
      @classroom = Classroom.new name: "name", clazzs: [Clazz.new]
      @classroom.should be_valid
    end

    it "should require name" do
      @classroom.name = ""
      @classroom.should_not be_valid
    end

    it "should require name" do
      @classroom.clazzs = []
      @classroom.should_not be_valid
    end
  end

  context "search" do
    before do
      clear_db
      @classroom1 = create_complete_classroom(1)
      @classroom2 = create_complete_classroom(2)
      @classroom3 = create_complete_classroom(3)
    end

    after do
      clear_db
    end

    it "should search_by_teachers_and_lessons" do
      Classroom.search_by_teachers_and_lessons([],[]).size.should == 3
      Classroom.search_by_teachers_and_lessons(@classroom1.teachers.all,[]).all.size.should == 1
      Classroom.search_by_teachers_and_lessons([],@classroom1.lessons.all).all.size.should == 1
      Classroom.search_by_teachers_and_lessons(@classroom1.teachers.all,@classroom1.lessons.all).all.size.should == 1
      Classroom.search_by_teachers_and_lessons(@classroom1.teachers.all + @classroom2.teachers.all,@classroom1.lessons.all).all.size.should == 2
      Classroom.search_by_teachers_and_lessons(@classroom1.teachers.all + @classroom2.teachers.all,@classroom1.lessons.all + @classroom3.lessons.all).all.size.should == 3
    end
  end
end
