require 'spec_helper'

describe "Classrooms" do
  before(:all) do
    clear_db
    @classroom = create_complete_classroom(10)
    @classroom2 = create_complete_classroom(20)
  end

  after do
    clear_db
  end

  describe "GET /classrooms" do
    it "show classrooms" do
      get classrooms_path
      response.status.should be(200)
      response.body.should include @classroom.name

      @classroom.students.each do |student|
        response.body.should include student.name
      end
      @classroom.teachers.each do |teacher|
        response.body.should include teacher.name
      end
      @classroom.lessons.each do |lesson|
        response.body.should include lesson.name
      end
    end
  end

  describe "GET /classrooms/filter" do
    it "should show all classrooms" do
      get classrooms_filter_path, filter: {teacher_ids: "", lesson_ids: ""}
      response.status.should be(200)

      response.body.should include @classroom.name
      response.body.should include @classroom2.name
    end

    it "should filter classrooms by teachers" do
      get classrooms_filter_path, filter: {teacher_ids: @classroom.teachers.first.id, lesson_ids: ""}
      response.status.should be(200)

      response.body.should include @classroom.name
      response.body.should_not include @classroom2.name
    end

    it "should filter classrooms by lessons" do
      get classrooms_filter_path, filter: {teacher_ids: "", lesson_ids: @classroom2.lessons.first.id}
      response.status.should be(200)

      response.body.should_not include @classroom.name
      response.body.should include @classroom2.name
    end
  end
end
