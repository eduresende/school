class Teacher < ActiveRecord::Base
  attr_accessible :name, :lessons_names
  attr_reader :lessons_names

  validates_presence_of :name

  has_many :clazzs
  has_many :lessons, :through => :clazzs
  has_many :classrooms, :through => :clazzs

  scope :sorted, order(:name)
  scope :query, lambda { |q| where("name like ?", "%#{q}%").sorted }

  def lessons_names=(names)
    self.lesson_ids = Lesson.ids_from_names(names)
  end

  def lessons_string
    lessons.map(&:name).join(", ")
  end

  def to_s
    name
  end

end
