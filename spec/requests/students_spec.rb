require 'spec_helper'

describe "Students" do
  before(:all) do
    Student.destroy_all
    @student = create_student
  end

  after(:all) do
    Student.destroy_all
  end

  describe "GET /students" do
    it "index" do
      get students_path
      response.status.should be(200)
      response.body.should include @student.name
    end
  end

  describe "POST /students" do
    it "should create" do
      post_via_redirect students_path, :student => {:name => "new student", registry: 21453}
      response.body.should include("new student")
    end
  end

  describe "GET /students/id" do
    it "should show student" do
      get student_path(@student.id)
      response.body.should include @student.name, @student.registry.to_s
    end
  end


end
