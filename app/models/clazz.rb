class Clazz < ActiveRecord::Base
  attr_accessible :name, :lesson, :teacher

  has_and_belongs_to_many :classroom
  belongs_to :lesson
  belongs_to :teacher

  def to_s
    "#{lesson} - #{teacher}"
  end
end
