# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130305013050) do

  create_table "classrooms", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "classrooms_clazzs", :id => false, :force => true do |t|
    t.integer "classroom_id"
    t.integer "clazz_id"
  end

  add_index "classrooms_clazzs", ["classroom_id"], :name => "index_classrooms_clazzs_on_classroom_id"
  add_index "classrooms_clazzs", ["clazz_id"], :name => "index_classrooms_clazzs_on_clazz_id"

  create_table "clazzs", :force => true do |t|
    t.string   "name"
    t.integer  "classroom_id"
    t.integer  "teacher_id"
    t.integer  "lesson_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "clazzs", ["classroom_id"], :name => "index_clazzs_on_classroom_id"
  add_index "clazzs", ["lesson_id"], :name => "index_clazzs_on_lesson_id"
  add_index "clazzs", ["teacher_id"], :name => "index_clazzs_on_teacher_id"

  create_table "lessons", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "students", :force => true do |t|
    t.string   "name"
    t.integer  "classroom_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "registry"
  end

  add_index "students", ["classroom_id"], :name => "index_students_on_classroom_id"

  create_table "teachers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
