require 'spec_helper'

describe Lesson do

  before do
    @english = Lesson.create!(name: "english")
    @sciences = Lesson.create!(name: "sciences")
    @geography = Lesson.create!(name: "geography")
  end

  after do
    Lesson.destroy_all
  end

  it "should query lessons or return new when do not exists" do
    Lesson.query("gli").should == [ @english ]
    Lesson.query("cie").should == [ @sciences ]
    Lesson.query("test").should == [{:id=>"<<<Test>>>", :name=>"Novo: \"Test\""}]
    Lesson.query("e").should include @english, @sciences, @geography
  end

  it "should return ids for lesson names and create new lesson" do
    Lesson.ids_from_names(@english.id.to_s).should == [@english.id.to_s]
    Lesson.ids_from_names("#{@english.id},#{@sciences.id}").should == [@english.id.to_s, @sciences.id.to_s]

    ids = Lesson.ids_from_names("#{@geography.id},<<<new>>>")
    ids.first.should == @geography.id.to_s
    new = Lesson.find_by_name("new")
    new.should_not be_nil
    ids.last.should == new.id.to_s
  end

  context "search" do
    before do
      clear_db
      @classroom1 = create_complete_classroom(1)
      @lesson_alone = create_lesson("alone")
    end

    after do
      clear_db
    end

    it "should return  with_teachers" do
      result = Lesson.with_teachers
      @classroom1.lessons.each do |lesson|
        result.should include lesson
      end
      result.should_not include @lesson_alone
    end

    it "should return with_teachers_and_no_classrooms" do
      Lesson.with_teachers_and_no_classrooms.all.size.should == 0

      lesson = create_lesson "new lesson"
      create_teacher [lesson]
      Lesson.with_teachers_and_no_classrooms.all.size.should == 1

      lesson2 = create_lesson "other new lesson"
      create_teacher [lesson, lesson2]
      Lesson.with_teachers_and_no_classrooms.all.size.should == 2
    end

  end
end
