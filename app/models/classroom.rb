class Classroom < ActiveRecord::Base
  #Turma
  attr_accessible :name, :student_ids, :clazz_ids, :clazzs

  validates_presence_of :clazzs, :name

  has_many :students
  has_and_belongs_to_many :clazzs
  has_many :lessons, :through => :clazzs
  has_many :teachers, :through => :clazzs
  before_destroy :clear_students

  scope :search_by_clazz_ids, lambda { |ids| joins(:clazzs).where(:clazzs => {:id => ids})}

  def self.search_by_teachers_and_lessons teachers, lessons
    where_string = ""

    unless teachers.empty?
      where_string << "clazzs.teacher_id IN (#{teachers.map(&:id).join(",")})"
    end

    unless lessons.empty?
      where_string << " OR " unless where_string.empty?
      where_string << "clazzs.lesson_id IN (#{lessons.map(&:id).join(",")})"
    end

    if where_string.empty?
      Classroom.scoped
    else
      Classroom.joins(:clazzs).where(where_string).uniq
    end
  end

  def to_s
  	name
  end

  def students_names
  	self.students.map(&:name).join(",")
  end

  def clazzs_names
  	self.clazzs.map{|clazz| clazz.lesson.to_s + " - " + clazz.teacher.to_s }.join("<br>")
  end

  private
  def clear_students
    self.students.clear
  end
end
