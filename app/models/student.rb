class Student < ActiveRecord::Base
  attr_accessible :name, :registry
  validates_uniqueness_of :registry
  validates_presence_of :name, :registry

  belongs_to :classroom

  scope :with_no_classrooms, where("classroom_id IS NULL")
  scope :sorted, order(:name)

  def to_s
    name
  end
end
