def create_classroom name = "classroom", clazzs, students
  Classroom.create! name: name, clazz_ids: clazzs.map(&:id), student_ids: students.map(&:id)
end

def create_complete_classroom index
  lesson1 = create_lesson "lesson1#{index}"
  lesson2 = create_lesson "lesson2#{index}"
  lesson3 = create_lesson "lesson3#{index}"

  teacher1 = create_teacher
  teacher2 = create_teacher
  teacher3 = create_teacher

  clazz1 = create_clazz lesson1, teacher1
  clazz2 = create_clazz lesson2, teacher2
  clazz3 = create_clazz lesson3, teacher3

  student = create_student(index*333)
  create_classroom "complete classroom#{index}", [clazz1,clazz2,clazz3], [student]
end

def clear_db
  Classroom.destroy_all
  Teacher.destroy_all
  Lesson.destroy_all
  Clazz.destroy_all
  Student.destroy_all
end