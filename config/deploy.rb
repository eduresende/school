$LOAD_PATH.unshift File.join(File.dirname(__FILE__), 'deploy')

require "bundler/capistrano"
require "capistrano_database"
require "rvm/capistrano"

set :rvm_ruby_string, 'ruby-1.9.3-p125@school'
set :rvm_type, :user

set :application, "school"

default_run_options[:pty] = true
set :repository,  "git@bitbucket.org:rodrigora/school.git"
set :scm, :git
set :branch, "master"
set :deploy_via, :remote_cache
set :use_sudo, false

set :user, "ubuntu"
set :deploy_to, "/home/#{user}/apps/#{application}"

set :rails_env, "production"

ssh_options[:forward_agent] = true

set :ip, "177.71.143.177"
role :web, ip
role :app, ip
role :db,  ip, :primary => true

set :unicorn_pid, "#{shared_path}/pids/unicorn.pid"

set :rake, "#{rake} --trace"

after "deploy", "rvm:trust_rvmrc"

namespace :deploy do
  task :start do
    top.unicorn.start
  end

  task :stop do
    top.unicorn.stop
  end

  task :restart do
    top.unicorn.reload
  end

  task :setup_config, roles: :app do
    run "sudo ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/"
  end

  task :set_rvm_version, :roles => :app, :except => { :no_release => true } do
    run "source /etc/profile.d/rvm.sh && rvm use #{rvm_ruby_string} --default"
  end

  task :ln_assets do
    run "#{shared_path}/assets #{latest_release}/public/assets"
  end

  namespace :db do
    task :seed do
      run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} db:seed"
    end

    task :migrate do
      run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} db:migrate"
    end

    task :schema_load do
      run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} db:schema:load"
    end
  end

  namespace :assets do
    task :precompile, :roles => :web, :except => {:no_release => true} do
      begin
        from = source.next_revision(current_revision)
        error = false
      rescue
        error = true
      end
      force_precompile = false
      if error or capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0 or force_precompile
        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      else
        logger.info " ***************** Skipping asset pre-compilation because there were no asset changes ****************"
      end
    end
  end
end

namespace :unicorn do
  desc "start unicorn server"
  task :start, :roles => :app do
    run "cd #{current_path} && bundle exec unicorn -E #{rails_env} -c config/unicorn.rb -D"
  end

  desc "stop unicorn server"
  task :stop do
    run "kill -s QUIT `cat #{unicorn_pid}`"
  end

  desc "restart unicorn"
  task :restart do
    top.unicorn.stop
    top.unicorn.start
  end

  desc "reload unicorn (gracefully restart workers)"
  task :reload do
    top.unicorn.restart
    #run "kill -s USR2 `cat #{unicorn_pid}`"
  end

  desc "reconfigure unicorn (reload config and gracefully restart workers)"
  task :reconfigure, :roles => :app do
    run "kill -s HUP `cat #{unicorn_pid}`"
  end
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end
