School::Application.routes.draw do
  resources :clazzs

  get "classrooms/clazz_view", :to => "classrooms#clazz_view"
  get "classrooms/filter", :to => "classrooms#filter"
  resources :classrooms

  get "lessons/query", :to => "lessons#query"
  get "lessons/query_without_new", :to => "lessons#query_without_new"
  resources :lessons

  get "teachers/query", :to => "teachers#query"
  get "teachers/for_lesson/:id", :to => "teachers#for_lesson"
  resources :teachers

  get "students/for_classroom/:id", :to => "students#for_classroom"
  resources :students

  root :to =>  "classrooms#index"
end
