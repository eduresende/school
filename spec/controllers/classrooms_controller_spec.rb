require 'spec_helper'


describe ClassroomsController do

  before(:all) do
    clear_db
    @teacher = Teacher.new name: "teacher"
    @lesson = Lesson.new name: "lesson"
    @student = Student.new name: "student", registry: 123

    @clazz = Clazz.new name: "class"
    @clazz.teacher = @teacher
    @clazz.lesson = @lesson

    @classroom = Classroom.new name: "classroom"
    @classroom.clazzs << @clazz
    @classroom.students << @student
    @classroom.save!
  end

  after(:all) do
    clear_db
  end

  def valid_attributes
    {name: "classroom", clazz_ids: [@clazz.id], student_ids: [@student.id] }
  end

  describe "GET index" do
    it "assigns all classrooms as @classrooms" do
      get :index
      assigns(:classrooms).should eq([@classroom])
    end
  end

  describe "GET new" do
    it "assigns a new classroom as @classroom" do
      get :new
      assigns(:classroom).should be_a_new(Classroom)
    end
  end

  describe "GET edit" do
    it "assigns the requested classroom as @classroom" do
      get :edit, {:id => @classroom.to_param}
      assigns(:classroom).should eq(@classroom)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Classroom" do
        expect {
          post :create, {:classroom => valid_attributes}
        }.to change(Classroom, :count).by(1)
      end

      it "assigns a newly created classroom as @classroom" do
        post :create, {:classroom => valid_attributes}
        assigns(:classroom).should be_a(Classroom)
        assigns(:classroom).should be_persisted
      end

    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved classroom as @classroom" do
        # Trigger the behavior that occurs when invalid params are submitted
        Classroom.any_instance.stub(:save).and_return(false)
        post :create, {:classroom => { "name" => "invalid value" }}
        assigns(:classroom).should be_a_new(Classroom)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Classroom.any_instance.stub(:save).and_return(false)
        post :create, {:classroom => { "name" => "invalid value" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested classroom" do
        classroom = Classroom.create! valid_attributes
        # Assuming there are no other classrooms in the database, this
        # specifies that the Classroom created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Classroom.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => classroom.to_param, :classroom => { "name" => "MyString" }}
      end

      it "assigns the requested classroom as @classroom" do
        put :update, {:id => @classroom.to_param, :classroom => valid_attributes}
        assigns(:classroom).should eq(@classroom)
      end

    end

    describe "with invalid params" do
      it "assigns the classroom as @classroom" do
        Classroom.any_instance.stub(:save).and_return(false)
        put :update, {:id => @classroom.to_param, :classroom => { "name" => "invalid value" }}
        assigns(:classroom).should eq(@classroom)
      end

      it "re-renders the 'edit' template" do
        Classroom.any_instance.stub(:save).and_return(false)
        put :update, {:id => @classroom.to_param, :classroom => { "name" => "invalid value" }}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested classroom" do
      expect {
        delete :destroy, {:id => @classroom.to_param}
      }.to change(Classroom, :count).by(-1)
    end

    it "redirects to the classrooms list" do
      delete :destroy, {:id => @classroom.to_param}
      response.should redirect_to(classrooms_url)
    end
  end

end
