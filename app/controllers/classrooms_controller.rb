class ClassroomsController < ApplicationController

  def filter
    filter = params[:filter]
    if filter[:clear]
      redirect_to :action => :index
    else
      @teachers = Teacher.find_all_by_id(filter[:teacher_ids].split(","))
      @lessons = Lesson.find_all_by_id(filter[:lesson_ids].split(","))
      @classrooms = Classroom.search_by_teachers_and_lessons @teachers , @lessons
      render "index"
    end

  end

  def clazz_view
    @clazz = Clazz.find_all_by_lesson_id_and_teacher_id(params[:lesson_id], params[:teacher_id]).first #TODO metodo
    render partial: "clazz_view", :locals => {:clazz => @clazz}
  end
  # GET /classrooms
  # GET /classrooms.json
  def index
    @classrooms = Classroom.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @classrooms }
    end
  end

  # GET /classrooms/new
  # GET /classrooms/new.json
  def new
    @classroom = Classroom.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @classroom }
    end
  end

  # GET /classrooms/1/edit
  def edit
    @classroom = Classroom.find(params[:id])
  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @classroom = Classroom.new(params[:classroom])

    respond_to do |format|
      if @classroom.save
        format.html { redirect_to classrooms_path }
        format.json { render json: @classroom, status: :created, location: @classroom }
      else
        format.html { render action: "new" }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /classrooms/1
  # PUT /classrooms/1.json
  def update
    @classroom = Classroom.find(params[:id])

    respond_to do |format|
      if @classroom.update_attributes(params[:classroom])
        format.html { redirect_to classrooms_path }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @classroom = Classroom.find(params[:id])
    @classroom.destroy

    respond_to do |format|
      format.html { redirect_to classrooms_url }
      format.json { head :no_content }
    end
  end
end
