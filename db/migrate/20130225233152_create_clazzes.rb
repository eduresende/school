class CreateClazzes < ActiveRecord::Migration
  def change
    create_table :clazzs do |t|
      t.references :teacher
      t.references :lesson
    end

    add_index :clazzs, :teacher_id
    add_index :clazzs, :lesson_id
  end
end
