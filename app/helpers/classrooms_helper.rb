module ClassroomsHelper

  def tables_lines classroom
    clazzs = classroom.clazzs.present? ? classroom.clazzs.size : 0
    students = classroom.students.present? ? classroom.students.size : 0
    clazzs > students ? clazzs : students
  end

  def teacher_for_line classroom, line
    if classroom.clazzs[line]
      classroom.clazzs[line].teacher
    end
  end

  def lesson_for_line  classroom, line
    if classroom.clazzs[line]
      classroom.clazzs[line].lesson
    end
  end
end
