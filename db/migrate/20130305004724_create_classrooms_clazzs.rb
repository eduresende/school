class CreateClassroomsClazzs < ActiveRecord::Migration
  def change
    create_table :classrooms_clazzs, :id => false do |t|
      t.references :classroom
      t.references :clazz
    end
    add_index :classrooms_clazzs, :classroom_id
    add_index :classrooms_clazzs, :clazz_id
  end

end
