require 'spec_helper'

describe "Teachers" do

  before(:all) do
    Teacher.destroy_all
    Lesson.destroy_all
    @lesson1 = create_lesson "lesson1"
    @lesson2 = create_lesson "lesson2"
    @teacher = create_teacher [@lesson1, @lesson2]
  end

  after(:all) do
    Teacher.destroy_all
    Lesson.destroy_all
  end
  describe "GET /teachers" do
    it "show teacher" do
      get teachers_path
      response.status.should be(200)
      response.body.should include @teacher.name, @lesson1.name, @lesson2.name
    end
  end

  describe "POST /teachers" do
    it "should create" do
      post_via_redirect teachers_path, :teacher => {:name => "another teacher", lessons_names: @lesson1.id}
      response.body.should include("another teacher")
      response.body.should include @lesson1.name
    end
  end

  describe "GET /students/id" do
    it "should show student" do
      get teacher_path(@teacher.id)
      response.body.should include @teacher.name, @lesson1.name, @lesson2.name
    end
  end
end
